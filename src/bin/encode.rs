use imagequilt::encode;
use std::error::Error;
use std::path::PathBuf;
use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short = 'i', long, value_parser)]
    input_image: PathBuf,

    #[clap(short = 'd', long, value_parser)]
    input_data: PathBuf,

    #[clap(short, long, value_parser)]
    output_image: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    let image = image::io::Reader::open(args.input_image)?.decode()?;
    let data = std::fs::read(args.input_data)?;

    let mut image = image.into_rgba8();

    encode(&mut image, data.len().try_into().unwrap(), move |encoder_data| {
        let byte = data.get(encoder_data.block_number as usize).copied().unwrap_or(0);
        let mut pixel = encoder_data.pixel;
        let odd_row = (encoder_data.block_coordinates.1 % 2) != 0;
        let odd_column = (encoder_data.block_coordinates.0 % 2) != 0;
        // Clear out all but most significant two bytes
        pixel[0] &= 0b11000000;
        pixel[1] &= 0b11000000;
        pixel[2] &= 0b11000000;
        // Set to middle of their ranges
        pixel[0] |= 0b00000100;
        pixel[1] |= 0b00000100;
        pixel[2] |= 0b00000100;
        // Evenness set
        if odd_row ^ odd_column {
            pixel[1] |= 0b00100000;
        }
        // red bit 2
        pixel[0] |= (0b10000000 & byte) >> 2;
        // blue bit 2
        pixel[2] |= (0b01000000 & byte) >> 1;
        // red bit 3
        pixel[0] |= (0b00100000 & byte) >> 1;
        // blue bit 3
        pixel[1] |= 0b00010000 & byte;
        // green bit 3
        pixel[2] |= (0b00001000 & byte) << 1;
        // red bit 4
        pixel[0] |= (0b00000100 & byte) << 1;
        // green bit 4
        pixel[1] |= (0b00000010 & byte) << 2;
        // blue bit 4
        pixel[2] |= (0b00000001 & byte) << 3;

        // check if the next heigher or lower available color is a better fit for the source image.
        for (source_subpixel, subpixel) in encoder_data.pixel.0.into_iter().zip(pixel.0.iter_mut()) {
            let next_heigher = subpixel.checked_add(0b01000000).unwrap_or_else(|| *subpixel | 0b00000111);
            let next_lower = subpixel.checked_sub(0b01000000).unwrap_or_else(|| *subpixel & 0b11111000);
            let closest = [next_lower, *subpixel, next_heigher].into_iter().min_by_key(move |&candidate| candidate.abs_diff(source_subpixel)).unwrap();
            *subpixel = closest;
        }
        pixel
    })?;
    image.save(args.output_image)?;
    Ok(())
}

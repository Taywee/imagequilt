#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub enum Error {
    InsufficientSize(InsufficientSize),
}

impl From<InsufficientSize> for Error {
    fn from(v: InsufficientSize) -> Self {
        Self::InsufficientSize(v)
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub struct InsufficientSize {
    pub available: (u32, u32),
    pub wanted: u64,
}

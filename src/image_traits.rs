//! Trait implementations for the image crate.

use crate::Image;
use crate::Pixel;

impl Pixel for image::Rgb<u8> {
    fn r(&self) -> u8 {
        self.0[0]
    }

    fn g(&self) -> u8 {
        self.0[1]
    }

    fn b(&self) -> u8 {
        self.0[2]
    }

    fn a(&self) -> u8 {
        u8::MAX
    }
}

impl Pixel for image::Rgba<u8> {
    fn r(&self) -> u8 {
        self.0[0]
    }

    fn g(&self) -> u8 {
        self.0[1]
    }

    fn b(&self) -> u8 {
        self.0[2]
    }

    fn a(&self) -> u8 {
        self.0[3]
    }
}

impl Image for image::RgbImage {
    type Pixel = image::Rgb<u8>;

    fn width(&self) -> u32 {
        self.width()
    }

    fn height(&self) -> u32 {
        self.height()
    }

    fn get_pixel(&self, x: u32, y: u32) -> Self::Pixel {
        *self.get_pixel(x, y)
    }

    fn set_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel) {
        *self.get_pixel_mut(x, y) = pixel;
    }
}

impl Image for image::RgbaImage {
    type Pixel = image::Rgba<u8>;

    fn width(&self) -> u32 {
        self.width()
    }

    fn height(&self) -> u32 {
        self.height()
    }

    fn get_pixel(&self, x: u32, y: u32) -> Self::Pixel {
        *self.get_pixel(x, y)
    }

    fn set_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel) {
        *self.get_pixel_mut(x, y) = pixel;
    }
}

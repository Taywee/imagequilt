pub mod error;

pub trait Pixel: Copy + Clone {
    fn r(&self) -> u8;
    fn g(&self) -> u8;
    fn b(&self) -> u8;
    fn a(&self) -> u8;
}

pub trait Image {
    type Pixel: Pixel;
    fn width(&self) -> u32;
    fn height(&self) -> u32;
    fn get_pixel(&self, x: u32, y: u32) -> Self::Pixel;
    fn set_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel);
}

#[cfg(feature = "image")]
mod image_traits;

#[derive(Default, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub struct Rgba {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Pixel for Rgba {
    fn r(&self) -> u8 {
        self.r
    }

    fn g(&self) -> u8 {
        self.g
    }

    fn b(&self) -> u8 {
        self.b
    }

    fn a(&self) -> u8 {
        self.a
    }
}

#[derive(Default, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct ImageBuffer {
    width: u32,
    height: u32,
    data: Vec<Rgba>,
}

impl Image for ImageBuffer {
    type Pixel = Rgba;

    fn width(&self) -> u32 {
        self.width
    }

    fn height(&self) -> u32 {
        self.height
    }

    fn get_pixel(&self, x: u32, y: u32) -> Self::Pixel {
        self.data[y as usize * self.width as usize + x as usize]
    }

    fn set_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel) {
        self.data[y as usize * self.width as usize + x as usize] = pixel;
    }
}

impl ImageBuffer {
    pub fn new(width: u32, height: u32, data: Vec<Rgba>) -> Self {
        Self {
            width,
            height,
            data,
        }
    }
}

impl std::ops::DerefMut for ImageBuffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl std::ops::Deref for ImageBuffer {
    type Target = Vec<Rgba>;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

pub struct EncoderData<I>
where
    I: Image,
{
    pub pixel: I::Pixel,
    pub pixel_coordinates: (u32, u32),
    pub block_coordinates: (u32, u32),
    pub block_number: u64,
    pub pixel_number: u64,
}

/// Call the encoder for each pixel, passing in the pixel itself, pixel coordinates, block number,
/// and block coordinates.
pub fn encode<I, E>(image: &mut I, needed_blocks: u64, mut encoder: E) -> Result<(), error::Error>
where
    I: Image,
    E: FnMut(EncoderData<I>) -> I::Pixel,
{
    let block_size: Result<_, error::Error> = (1..=image.width().min(image.height()))
        .rev()
        .find(|&block_size| {
            let blocks_width = image.width() / block_size;
            let blocks_height = image.height() / block_size;
            blocks_width as u64 * blocks_height as u64 >= needed_blocks
        })
        .ok_or(
            error::InsufficientSize {
                available: (image.width(), image.height()),
                wanted: needed_blocks,
            }
            .into(),
        );

    let block_size = block_size?;
    let blocks_width = image.width() / block_size;
    let blocks_height = image.height() / block_size;

    // Spare horizontal pixels
    let spare_width = image.width() - (block_size * blocks_width);
    let spare_height = image.height() - (block_size * blocks_height);

    let gutter_left = spare_width / 2;
    let gutter_right = spare_width - gutter_left;
    let gutter_top = spare_height / 2;
    let gutter_bottom = spare_height - gutter_top;

    for block_y in 0..blocks_height {
        for block_x in 0..blocks_width {
            let block_number = block_y as u64 * blocks_width as u64 + block_x as u64;
            let y_pixels_offset = if block_y == 0 {
                0
            } else {
                block_y * block_size + gutter_top
            };
            let x_pixels_offset = if block_x == 0 {
                0
            } else {
                block_x * block_size + gutter_left
            };
            let block_height = match block_y {
                0 => block_size + gutter_top,
                height if height == blocks_height - 1 => block_size + gutter_bottom,
                _ => block_size,
            };
            let block_width = match block_x {
                0 => block_size + gutter_left,
                width if width == blocks_width - 1 => block_size + gutter_right,
                _ => block_size,
            };

            for pixel_y in y_pixels_offset..(y_pixels_offset + block_height) {
                for pixel_x in x_pixels_offset..(x_pixels_offset + block_width) {
                    let pixel_number = pixel_y as u64 * image.width() as u64 + pixel_x as u64;
                    let pixel = encoder(EncoderData {
                        pixel: image.get_pixel(pixel_x, pixel_y),
                        pixel_coordinates: (pixel_x, pixel_y),
                        block_coordinates: (block_x, block_y),
                        block_number,
                        pixel_number,
                    });
                    image.set_pixel(pixel_x, pixel_y, pixel);
                }
            }
        }
    }
    Ok(())
}
